This folder holds files that are deployed in our infrastructure.

Unless stated otherwise these files are manually deployed.

## nginx/ci.nunet.io.conf

NGINX config file describing the ci reports web server. Deployed at dev.nunet.io.

## cronjob/cron.daily/rotate_reports

Cronjob file to remove reports older than 90 days. Deployed at dev.nunet.io.
