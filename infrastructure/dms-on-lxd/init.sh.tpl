#!/usr/bin/env bash
exec > >(tee /var/log/init.log) 2>&1
set -euo pipefail

apt update
apt install -y /opt/dms.deb

%{ if enable_nebula == true ~}
curl -u ${nebula_user.username}:${nebula_user.password} -s https://lh.nunet.io/nunet-lxd002/installnebula.sh | bash -s -- -u ${nebula_user.username} -p ${nebula_user.password} -s -a
%{ endif ~}

touch /opt/done

