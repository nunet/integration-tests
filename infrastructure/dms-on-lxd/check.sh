#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo executing dms-on-lxd pre-run checks...
if ! [[ -f $SCRIPT_DIR/config.yml ]] && { ! [[ -v DMS_ON_LXD_ENV ]] || ! [[ -v DMS_DEB_FILE ]] ; }; then
    echo ERROR: config.yml file missing, but DMS_ON_LXD_ENV not set
    echo either create a config.yml file manually or configure DMS_ON_LXD_ENV and DMS_DEB_FILE to populate a config file automatically
    echo for more information please refer to "$SCRIPT_DIR/USAGE.md"
    exit 1
fi

echo all checks passed

