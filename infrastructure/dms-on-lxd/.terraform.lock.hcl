# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/local" {
  version = "2.5.2"
  hashes = [
    "h1:6lS+5A/4WFAqY3/RHWFRBSiFVLPRjvLaUgxPQvjXLHU=",
    "zh:25b95b76ceaa62b5c95f6de2fa6e6242edbf51e7fc6c057b7f7101aa4081f64f",
    "zh:3c974fdf6b42ca6f93309cf50951f345bfc5726ec6013b8832bcd3be0eb3429e",
    "zh:5de843bf6d903f5cca97ce1061e2e06b6441985c68d013eabd738a9e4b828278",
    "zh:86beead37c7b4f149a54d2ae633c99ff92159c748acea93ff0f3603d6b4c9f4f",
    "zh:8e52e81d3dc50c3f79305d257da7fde7af634fed65e6ab5b8e214166784a720e",
    "zh:9882f444c087c69559873b2d72eec406a40ede21acb5ac334d6563bf3a2387df",
    "zh:a4484193d110da4a06c7bffc44cc6b61d3b5e881cd51df2a83fdda1a36ea25d2",
    "zh:a53342426d173e29d8ee3106cb68abecdf4be301a3f6589e4e8d42015befa7da",
    "zh:d25ef2aef6a9004363fc6db80305d30673fc1f7dd0b980d41d863b12dacd382a",
    "zh:fa2d522fb323e2121f65b79709fd596514b293d816a1d969af8f72d108888e4c",
  ]
}

provider "registry.opentofu.org/terraform-lxd/lxd" {
  version     = "2.3.0"
  constraints = "2.3.0"
  hashes = [
    "h1:xG63tLa4hXmce3QVdvPEiJ1Xk4u0fteyEWEPnQDhfwI=",
    "zh:18e632c7f343080437ec8f4885cd2cf9d687c1d9544db88b221c1e7deae96447",
    "zh:2fb7028c5d3432eed959b00f25316bb5938ef983662a2fcfbcca2d6452cfbdac",
    "zh:4b61f7d575c63cd97561b73f63b0a6209c63f4bc036d325114b7d298c076b1eb",
    "zh:5b1d6fb55db7b49144e9a6a64920228e0ce7fc2536326a4cd1c32d492d5a515a",
    "zh:6da4a07214cd07a26e98125be98b62413b7d8de6068aeee9ca69c76509fb0503",
    "zh:7f7f15c6131f59df98b47f41089c821f7acd2fa17d79c8db9b9a23afceb2cdd5",
    "zh:851d12172f9881fd444f8bb51dda6a648cb28dfc31bc153f56cb3c0d2cd62ff2",
    "zh:8571d137b6e12d2519e13590bd9ef0813a3de9f80e2216930020a22b6705a4cb",
    "zh:b273efaee8bbc27d4c789f0f5e5eae68c023e368eaec4bf71b29aaf866533940",
    "zh:b5bdb9e4f09e3526d30d19734e10290cdfe9f56094901d7a9b5cd76c7856262e",
    "zh:baa878b6fc05316dcffd658b66cc76b453fca6a795397ab95826b8da4ef459f0",
    "zh:c785eaa6b230663c64d4afe98311e0585d2e834faf896ca04d203e554aec547f",
    "zh:f0526772086163435d1031f375551cbdaed57402166a748499fbf887ed3a8c0e",
    "zh:f2a00e00084941fee2b1b0f200bab78f2c425343c5a79caf3caa55ecb86b6074",
  ]
}
