variable "env_name" {
  description = "Name of the environment to draw configuration from"
  type        = string
  default     = null
}

variable "lxd_install_source" {
  description = "Tells terraform whether lxd has been installed via snap"
  # has to be string instead of bool because the value will be populated via env var TF_VAR_lxd_install_source
  type    = string
  default = "other"
}
