# DMS on LXD Environment Usage Guide

## Using Environment-Specific Configurations

The scripts in this directory support running multiple isolated DMS-on-LXD environments by using the `DMS_ON_LXD_ENV` environment variable. This variable controls:

- The config file name (`config-{ENV}.yml`)
- The Terraform workspace name
- The instance name prefix
- The environment-specific LXD token file

### Required Environment Variables

When using `DMS_ON_LXD_ENV`, you must set:

- `DMS_ON_LXD_ENV`: Your environment name (e.g., "staging", "testing")
- `DMS_DEB_FILE`: Path to the DMS debian package file

### Optional Environment Variables

You can customize the environment further with:

- `INSTANCE_TYPE`: Defaults to "virtual-machine"
- `DISTRO_VERSION`: Defaults to "24.04"
- `DISTRO_NAME`: Defaults to "ubuntu"
- `LXD_LOCAL_PORT`: LXD API port (defaults to 8443)
- `LXD_LIMIT_MEM`: Sets LXD VM memory limit (defaults to 4GB)
- `LXD_LIMIT_CPU`: Sets LXD VM VCPU limit (defaults to 2)
- `DMS_ON_LXD_DOCKER`: Controls whether to run commands in Docker (defaults to "true")
- `DMS_ON_LXD_VERBOSE`: Verbose mode prints more info to the terminal (defaults to "false")
- `DMS_ON_LXD_APPLY_DOCKER_PATCH`: Apply iptables fix for docker and lxd compatibility (defaults to "true")
- `MAX_ITER`: Maximum number of iterations to wait for DMS (defaults to 30)
- `POLLING_INTERVAL_SECONDS`: Time to wait between DMS checks (defaults to 10)
- `TF_APPLY_STRICT`: If set, destroys infrastructure on apply failure

### Example Usage

To create a testing environment:

```bash
export DMS_ON_LXD_ENV="testing"
export DMS_DEB_FILE="path/to/nunet-dms.deb"
./make.sh    # Creates and provisions the infrastructure
./destroy.sh # Destroys the infrastructure when done
```

This will:
1. Generate a `config-testing.yml` file if it doesn't exist
2. Create and select a "testing" Terraform workspace
3. Create instances with the prefix "testing-"
4. Store the LXD token in `.testing-token`
5. Wait for DMS to be ready on all instances

### Multiple Environments

You can maintain multiple environments by using different `DMS_ON_LXD_ENV` values:

```bash
# Staging environment
export DMS_ON_LXD_ENV="staging"
./make.sh

# Development environment
export DMS_ON_LXD_ENV="dev"
./make.sh
```

Each environment will have its own:
- Configuration file
- Terraform workspace
- Instance naming
- LXD token
