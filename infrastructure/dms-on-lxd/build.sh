#!/usr/bin/env bash
set -euo pipefail

terraform init

source lib.sh
generate-config
test-and-add-remote-lxd-hosts

terraform plan -out=plan.out

