# static_analysis

Contains static analysis of the code quality using a number of tools that check and report code quality level. Code quality results for each run of this job are displayed via the gitlab interface.

Implementation: https://gitlab.com/nunet/nunet-infra/-/blob/develop/ci/templates/Jobs/Code-Quality.gitlab-ci.yml