from lib.python.behave.lxd import exec_cmd, assert_cmd
import time


def grant(context, instance, grantor, grantee):
    grantee_did = context.users[grantee]["did"]
    out = exec_cmd(
        context,
        instance,
        f"nunet cap grant --context {grantor} --cap /dms/deployment --cap /public --cap /broadcast --topic /nunet --duration 10h {grantee_did}",
    )
    assert_cmd(out)

    assert "tok" in out.stdout, f"stdout: {out.stdout}"
    grant_token = out.stdout.strip()
    return grant_token


def anchor(context, instance, user, kind, arg):
    command = ""
    if kind == "require" or kind == "provide":
        command = f"nunet cap anchor --context {user} --{kind} '{arg}'"
    else:
        if kind == "root":
            command = f"nunet cap anchor --context {user} --{kind} {arg}"
    out = exec_cmd(
        context,
        instance,
        command,
    )
    assert_cmd(out)


def delegate(context, instance, user, node):
    # currently only supporting delegating capabilities
    # to nodes which belong to the same user
    assert node in context.users[user]["nodes"]
    node_did = context.users[user]["nodes"][node]["did"]

    out = exec_cmd(
        context,
        instance,
        f"nunet cap delegate --context {user} --cap /dms/deployment --cap /public --cap /broadcast --topic /nunet --duration 8h {node_did}",
    )
    assert_cmd(out)

    assert "tok" in out.stdout, f"stdout: {out.stdout}"
    delegate_token = out.stdout.strip()
    return delegate_token


def run(context, instance, user, node):
    out = exec_cmd(
        context,
        instance,
        f"GOLOG_LOG_LEVEL=debug nunet run --context {node} &> /tmp/{node}.log & disown -h",
    )
    print(out.stdout)
    assert_cmd(out)
    context.users[user]["nodes"][node]["running"] = True
    time.sleep(3)
