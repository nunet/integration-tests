# Quick Setup Guide

This guide provides comprehensive instructions for setting up and running the NuNet functional test suite. It covers both standalone tests (testing individual DMS instances) and distributed tests (testing network interactions between multiple DMS nodes).

## Prerequisites

### Install Docker

- https://docs.docker.com/engine/install/

### Install LXD

- https://documentation.ubuntu.com/lxd/en/latest/installing/
- `lxd init` accepting all defaults if possible. You might need to change `ZFS` storage type to `dir`.

### Setup LXD API Access
1. Enable the API:
```bash
sudo lxc config set core.https_address "[::]:8443"
```

## Setup DMS-on-LXD Environment

1. Build or download the required dms debian package.

2. Set required environment variables:
```bash
export DMS_ON_LXD_ENV="local"
export DMS_DEB_FILE="path/to/nunet-dms.deb"
```

3. Run the deployment:
```bash
# from test-suite root dir
cd infrastructure/dms-on-lxd
./make.sh
```

## Running Tests

The following examples show how to run either distributed or standalone tests overriding the feature files.

To list all the feature files use something like `fd`:

```shell
fd .feature
```

### Standalone Tests
Run all standalone tests:
```bash
cd stages/functional_tests
./feature_environment_scripts/run-standalone-tests.sh
```

Run a specific feature file:
```bash
cd stages/functional_tests
./feature_environment_scripts/run-standalone-tests.sh features/example.feature
```

You can find all standalone feature tests for the `actor-cmd` folder and pass them to the test script:

```shell
cd stages/functional_tests
./feature_environment_scripts/run-standalone-tests.sh "$(fd .feature | grep standalone | grep actor-cmd)"
```

### Distributed Tests 
Run all distributed tests:
```bash
cd stages/functional_tests
./feature_environment_scripts/run-distributed-tests.sh
```

Run a specific feature file:
```bash
cd stages/functional_tests
./feature_environment_scripts/run-distributed-tests.sh features/example.feature
```

You can find all distributed feature tests for the `actor-cmd` folder and pass them to the test script:

```shell
cd stages/functional_tests
./feature_environment_scripts/run-distributed-tests.sh "$(fd .feature | grep distributed | grep actor-cmd)"
```

## Cleanup

When done testing:
```bash
cd infrastructure/dms-on-lxd
./destroy.sh
```
