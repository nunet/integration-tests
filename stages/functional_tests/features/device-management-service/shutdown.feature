Feature: Ability to shutdown running ensemble on demand
    As a user
    I want to be able
    To selectively shutdown a deployed ensemble

    ## Please add scenarios below in Gherkin synax
    ## Or in plain English explaining their logic as comments

    Background:
        Given Alice is a service provider
        And Bob is a compute provider
        And Charlie is a compute provider
        And Alice trust Bob 
        And Alice trust Charlie
        And Bob and Charlie both have their resources onboarded

    @wip @capability
    @standalone
    Scenario: Shutdown a single ensemble with one allocation in a docker container
        Given Alice has deployed a single ensemble with one allocation in a docker container
        And the docker container is running
        When Alice requests to shut down the ensemble
        Then the allocation should be terminated 

    @wip @capability
    @distributed
    Scenario: Shutdown a single ensemble with two allocations on two different nodes
        Given Alice has deployed an ensemble with one allocation on Bob 
        And Alice has deployed an ensemble with one allocation on Charlie 
        When Alice requests to shut down the ensemble
        Then the resources should be deallocated on Bob and Charlie 

    @wip @capability
    @distributed
    Scenario: Shutdown a single ensemble with two allocations on the same node
        Given Alice has deployed a single ensemble with two allocations on Bob node
        When Alice requests to shut down the ensemble
        Then the resource manager should release all allocations resources on Bob

    @wip @capability
    @distributed
    Scenario: Request times out during shutdown due to network issues
        Given Alice has deployed an ensemble 
        And the system is experiencing network issues
        When Alice requests to shut down the ensemble
        Then the system returns an error message

    @wip @capability
    @distributed
    Scenario: Graceful shutdown of a running ensemble
        Given an ensemble is running with allocations on multiple nodes
        When Alice requests to concurrently shutdown the ensemble 
        Then the ensemble should be successfully shutdown without overlapping operations


    @wip @capability
    @distributed
    Scenario: Shutdown request fails due to missing permissions
        Given Alice does not have the required permissions to shut down the ensemble
        When Alice requests to shut down the ensemble
        Then the system should return an error message 
    