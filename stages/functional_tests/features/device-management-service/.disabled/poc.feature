# feature definitions for functional tests to be run in lxd environment
Feature: onboard multiple instances via lxd

  Background:
    Given dms lxd instances are up
    And nunet is available on all instances

  Scenario: Check that all dms instances can onboard
    When nunet onboard is executed in all instances
    Then all instances should onboard

  Scenario: All instances should see each other in the peer list
    When listing all peers in each instance
    Then every other instance id should appear in the list

  Scenario: Querying device status api
    When sending a get to "/api/v1/device/status"
    Then status code must be 200
    And response body should be
      """
      {"online":true}
      """

  Scenario: Check that all dms instances can offboard
    When nunet offboard is executed in all instances
    Then all instances should offboard


  Scenario: Test-1
    When sending a get to "/api/v1/device/status" on 1 dms instance
    Then status code must be 201
    And response body should be
      """
      {"online":true}
      """