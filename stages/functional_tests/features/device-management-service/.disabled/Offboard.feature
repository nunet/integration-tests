@skip
Feature: Offboard Endpoint

  Background:
    Given dms lxd instances are up and running
    And dms service is active on the target machine
    And the API base URL is "http://localhost:9999/api/v1"

#  ________________________________________________
# Moved from - api-tests/onboarding_api.feature
#  ________________________________________________
  @setup_onboard
  Scenario: Verify the off-board api response when the device is on-boarded
    When a DELETE request is made to the "onboarding/offboard" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the value of "message" attribute in the response body should be "device successfully offboarded"


#  Scenario: Successfully offboard a device
#    Given the DMS is installed and the Offboard endpoint is available - Done. No need to mention the api end point. It's implicit.
#    And the device is onboarded - Done
#    When a request is made to /onboarding/offboard - Done
#    Then 'device offboarding started' message in "offboardingStart.message" format is sent to Elasticsearch - ??
#    And 'force' parameter should be checked and parsed - ??
#    And check if device is onboarded - not required as is violating the Given condition
#    Then the metadataV2.json file should be deleted - Can be automated but we need to do such validations
#    And available resources in the database should be deleted - ??
#    And the libp2p node should be properly shut down - ??
#    Then 'device offboarded successfully' in "successfulOffboarding.message" format message is sent to Elasticsearch - ??
#    Then the response status code should be 200 - Done
#    And the response should be in JSON format - Can be automated.But as per industry practice it is not need to check.
#    And the response should contain a success message - Done
#    And the response should contain the force parameter value - can be done
#    And the response should not contain an error message- Not required as we already check for the success message.



 #  ________________________________________________
#  Moved from - api-tests/dms_api_wo_onboard.feature
#  ________________________________________________
  @setup_offboard
  Scenario: Verify the off-board api response when the device is not onboarded
    When a DELETE request is made to the "onboarding/offboard" endpoint
    Then the response status code should be 500
    And the response status message should be "Internal Server Error"
    And the value of "error" attribute in the response body should be "machine is not onboarded"


#  Scenario: Error when the device is not onboarded
#    Given the DMS is installed and the Offboard endpoint is available - Done. No need to mention the api end point. It's implicit.
#    And the device is not onboarded - Done
#    When a request is made to /onboarding/offboard - Done
#    Then 'device offboarding started' message in "offboardingStart.message" format is sent to Elasticsearch - ??
#    And 'force' parameter should be checked and parsed - ??
#    And check if device is onboarded - not required as is violating the Given condition
#    Then the response status code should be 400 - Done
#    And the response should be in JSON format - Can be automated
#    And the response should contain an error message about the machine not being onboarded - Done
#    And 'offboarding error : machine not onboarded' message in "offboardingError.message" format should be sent to Elasticsearch  - ??


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Error when the force parameter is not provided and there is an error retrieving the onboarding status
    Given the DMS is installed and the Offboard endpoint is available
    And there is a error in onboarding status check
    And the force parameter is not provided or set to false 
    When a request is made to /onboarding/offboard
    Then 'device offboarding started' message in "offboardingStart.message" format is sent to Elasticsearch
    And 'force' parameter should be checked and parsed
    Then the response status code should be 400
    And the response should be in JSON format
    And the response should contain an error message about the problem with state
    And 'offboarding error: ' + onboard_status_error in "offboardingError.message" format should be sent to Elasticsearch


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Forced offboarding when there is an error retrieving the onboarding status
    Given the DMS is installed and the Offboard endpoint is available
    And there is a error in onboarding status check
    And the force parameter is set to true
    When a request is made to /onboarding/offboard
    Then 'device offboarding started' message in "offboardingStart.message" format is sent to Elasticsearch
    And 'force' parameter should be checked and parsed
    Then error message to user about onboarding status is shown
    And forced offboarding message is shown to the user 
    And the metadataV2.json file should be deleted
    And available resources in the database should be deleted
    And the libp2p node should be properly shut down
    And 'device offboarded successfully' in "successfulOffboarding.message" format message is sent to Elasticsearch
    Then the response status code should be 200
    And the response should be in JSON format
    And the response should contain a success message and force parameter
    And the device should be successfully offboarded
    And the response should not contain an error message


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Error when failing to delete metadata file and force parameter is false
    Given the DMS is installed and the Offboard endpoint is available
    And the device is onboarded
    And there is a problem deleting the metadata file
    And force parameter is set as False
    When a request is made to /onboarding/offboard
    Then 'device offboarding started' message in "offboardingStart.message" format is sent to Elasticsearch
    And 'force' parameter should be checked and parsed
    Then the response status code should be 500
    And the response should be in JSON format
    And the response should contain an error message about failing to delete the metadata file


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Error when failing to delete metadata file and force parameter is true
    Given the DMS is installed and the Offboard endpoint is available
    And the device is onboarded
    And there is a problem deleting the metadata file
    And force parameter is set as True
    When a request is made to /onboarding/offboard
    Then 'device offboarding started' message in "offboardingStart.message" format is sent to Elasticsearch
    And 'force' parameter should be checked and parsed
    Then error message about failing to delete the metadata file should be shown to the user
    And forced offboarding message should be shown to the user
    And available resources in the database should be deleted
    And the libp2p node should be properly shut down
    And 'device offboarded successfully' message in "successfulOffboarding.message" format is sent to Elasticsearch
    Then the response status code should be 200
    And the response should be in JSON format
    And the response should contain a success message and force parameter
    And the device should be successfully offboarded
    And the response should not contain an error message


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Error when failing to delete available resources from the database
    Given the DMS is installed and the Offboard endpoint is available
    And the device is onboarded
    And there is a problem deleting available resources from the database
    When a request is made to /onboarding/offboard
    Then 'device offboarding started' message in "offboardingStart.message" format is sent to Elasticsearch
    And 'force' parameter should be checked and parsed
    Then if there is error in deleting database
    Then error should be logged to the user
    Then if no rows are affected in database
    Then 'No rows affected' message should be shown to the user
    And if force parameter is false
    Then Offboarding process should stop and return back to user
    And if force parameter is true
    Then the libp2p node should be properly shut down
    And 'device offboarded successfully' message in "successfulOffboarding.message" format is sent to Elasticsearch
    Then the response status code should be 200
    And the response should be in JSON format
    And the response should contain a success message and force parameter
    And the device should be successfully offboarded
    And the response should not contain an error message


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Error when failing to properly shut down the libp2p node and force parameter is false
    Given the DMS is installed and the Offboard endpoint is available
    And the device is onboarded
    And there is a problem shutting down the libp2p node
    When a request is made to /onboarding/offboard
    Then 'device offboarding started' message in "offboardingStart.message" format is sent to Elasticsearch
    And 'force' parameter should be checked and parsed
    Then the response status code should be 500
    And the response should be in JSON format
    And the response should contain an error message about failing to shut down the libp2p node
