## Functional test matrix

The first column contains different ensemble specification and the rows contain features that are tested on each configuration.

| Ensemble configuration | Deployed | Communication | Constraints | Shutdown | Restart | Capabilities | Restricted network | Resources management |
|-- |-- |-- |-- |-- |-- |-- |-- |-- |
| 1. Single ensemble deployment | | | | | | | | |
| 1.1 One allocation docker | | | | | | | | |
| 1.2 Two allocations on two different nodes | | | | | | | | |
| 1.3 Two allocations on the same node | | | | | | | | |
| 2. Two ensembles deployment | | | | | | | | |
| 2.1 One allocation docker | | | | | | | | |
| 2.1.1 From the same provider | | | | | | | | |
| 2.1.2 From diffent providers | | | | | | | | |
| 2.2 Two allocations docker | | | | | | | | |
| 2.2.1 From the same provider | | | | | | | | |
| 2.2.2 From diffent providers | | | | | | | | |