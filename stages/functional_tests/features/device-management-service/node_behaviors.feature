Feature: Node behaviors
	As a node in the network
	I want to be able connect to other nodes
	So that I can call behaviors on them

	Background:
		Given NuNet is an organization
		And Alice has a node
		And Bob has a node

	# This means calling a behavior from the user's context 
	# to a trusted node as destination
	@capability
	@standalone
	Scenario: A user calling behaviors on a node 
		Given Alice and Alice's node trust each other
		When Alice calls a behavior on Alice's node
		Then Alice should be authorized

	# This means calling a behavior from the first node's context 
	# to another trusted node as destination
	@capability
	@distributed
	Scenario: A node calling behaviors through another node
		Given Bob's node and Alice's node trust each other
		When Alice's node calls a behavior on Bob's node
		Then Alice's node should be authorized

	# TODO: Two nodes from same user
	
	# This means calling a behavior from the first node's context 
	# to another node as destination (being both authorized by NuNet)
	@capability
	@distributed
	Scenario: Two nodes (both authorized by NuNet) calling behaviors on each other
		Given NuNet authorizes Alice
		And NuNet authorizes Bob
		When Alice's node calls a behavior on Bob's node
		Then Alice's node should be authorized

	# This means calling a behavior from the first node's context 
	# to another node as destination (none being authorized by NuNet)
	@capability
	@distributed
	Scenario: Two nodes (neither authorized by NuNet) calling behaviors on each other
		Given Alice and Alice's node trust each other
		And Bob and Bob's node trust each other
		When Alice's node calls a behavior on Bob's node
		Then Alice's node should not be authorized
