Feature: Two ensembles deployment with one allocation Docker
    Two ensembles require one allocation docker container each.
    Two ensembles require two allocation docker container each.
    Allocations can be made on nodes provided by the same or different providers.

  @wip @capability @distributed
  Scenario: Successful allocation of two ensembles from the same provider
    Given Alice with onboarded resources "Alice_resources"
    When "two_alloc_node.yaml" are requested for deployment
    Then the free resources should be updated

  @wip @capability @distributed
  Scenario: Successful allocation of two ensembles from different providers
    Given Alice with onboarded resources as "Alice_resources"
    And Sam with onboarded resources as "sam_resources"
    When "two_alloc_two_node.yaml" are requested for deployment
    Then the free resources should be updated

  @wip @capability @distributed
  Scenario: Allocation fails due to one provider being unavailable
    Given Alice with onboarded resources as "Alice_resources"
    And Sam with onboarded resources as "sam_resources"
    And Sam is offline
    When "two_alloc_two_node.yaml" is requested for deployment
    Then the allocation should fail with the error "Provider B unavailable"

  @wip @capability @distributed
  Scenario: Deallocation of resources after job completion
    Given Alice and Sam with committed "two_alloc_two_node.yaml"
    When "two_alloc_two_node.yaml" is finished executing
    Then both Alice and Same offboard resources
    # 2.2 Two Allocations Docker

  @wip @capability @distributed
  Scenario: Successful allocation for two ensembles with two allocations Docker on nodes from the same provider
    Given Alice  onboarded resources as "Alice_resources"
    And Sam with onboarded resources as "sam_resources"
    When Two allocation Docker containers "two_alloc_node" are provisioned with:
      | AllocationID | CPU | GPUs | RAM | Disk |
      | A01          |   4 |    1 |  16 |  100 |
      | A01          |   4 |    1 |  16 |  100 |
      | A02          |   8 |    2 |  16 |   50 |
      | A02          |   8 |    2 |  16 |   50 |
    Then the free resources should be updated

  @wip @capability @distributed
  Scenario: Successful allocation for two ensembles on nodes from different providers
    Given Alice with onboarded resources as "Alice_resources"
    And Sam with onboarded resources as "sam_resources"
    When two allocation Docker containers "two_alloc_two_node.yaml" are provisioned with:
      | AllocationID | CPU | GPUs | RAM | Disk |
      | A01          |   4 |    1 |  16 |  100 |
      | A01          |   4 |    1 |  16 |  100 |
      | A02          |   8 |    2 |  16 |   50 |
      | A02          |   8 |    2 |  16 |   50 |
    Then the free resources should be updated

  @wip @capability @distributed
  Scenario: Allocation fails due to insufficient GPU resources across all nodes
    Given Alice and Sam with free onboarded resources:
      | Node | Provider | CPU | GPUs | RAM | Disk |
      | N1   | A        |  16 |    1 |  64 |  500 |
      | N2   | B        |  16 |    1 |  48 |  300 |
    When four allocation Docker containers "two_alloc_two_node.yaml" are provisioned with:
      | AllocationID | CPU | GPUs | RAM | Disk |
      | A01          |   4 |    4 |  16 |  100 |
      | A01          |   4 |    4 |  16 |  100 |
      | A02          |   8 |    2 |  16 |   50 |
      | A02          |   8 |    2 |  16 |   50 |
    Then the allocation should fail with the error "Insufficient GPU resources across nodes"

  @wip @capability @distributed
  Scenario: Allocation succeeds but one ensemble is split across multiple nodes
    Given Alice with onboarded resources:
      | Node | CPU | GPUs | RAM | Disk |
      | N1   |   8 |    2 |  32 |  200 |
      | N2   |   8 |    2 |  32 |  200 |
      | N3   |   8 |    2 |  32 |  200 |
    When four allocation Docker containers "two_alloc_two_node.yaml" are provisioned with:
      | AllocationID | CPU | GPUs | RAM | Disk |
      | A01          |   4 |    4 |  16 |  100 |
      | A01          |   4 |    4 |  16 |  100 |
      | A02          |   8 |    2 |  16 |   50 |
      | A02          |   8 |    2 |  16 |   50 |
    Then the free resources should be updated to:
      | Node | CPU | GPUs | RAM | Disk |
      | N1   |   0 |    0 |   0 |    0 |
      | N2   |   6 |    1 |  24 |  150 |
      | N3   |   6 |    1 |  24 |  150 |

  @wip @capability @distributed
  Scenario: Allocation fails due to resource contention (two ensembles allocation requested simultaneously)
    Given Alice with onboarded resources:
      | Node | CPU | GPUs | RAM | Disk |
      | N1   |  16 |    4 |  64 |  500 |
      | N2   |  12 |    3 |  48 |  300 |
    When two allocation "two_alloc_node.yaml"  requests are made simultaneously with:
      | AllocationID | Node | CPU | GPUs | RAM | Disk |
      | A01          | N1   |   8 |    2 |  32 |  250 |
      | A02          | N1   |   8 |    2 |  32 |  250 |
    Then the allocation for one ensemble should fail with the error "Resource contention on Node N1"
