@wip @standalone
Feature: base security tests

    Scenario: execute linux baseline security checks with ssh
        Given we have a target virtual machine
        When I execute "echo hello world" over ssh
        Then "hello world" should be printed to "stdout"
        And the operating system should pass security checks
