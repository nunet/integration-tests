from lib.python.behave.lxd import exec_command_lxd_instance, exec_cmd
from behave import given, then, when
import ipaddress


@given("we have a target virtual machine")
def get_target_machine(context):
    instance = context.instances[0]
    try:
        ip_address = ipaddress.ip_address(instance)
        assert ip_address.is_private, f"expected {instance} to be a private ipaddress"
    except:
        print(f"expected {instance} to be an IP")
        raise


@when('I execute "{shell_cmd}" over ssh')
def execute_shell_over_ssh(context, shell_cmd):
    instance = context.instances[0]

    out = exec_cmd(context, instance, shell_cmd)

    context.output = out


@then('"{expected_output}" should be printed to "{stream_name}"')
def check_shell_output(context, expected_output, stream_name):
    actual_output = getattr(context.output, stream_name).strip()
    assert (
        actual_output == expected_output
    ), f"expected '{expected_output}', got '{actual_output}'"


@then("the operating system should pass security checks")
def os_security_check(context):
    instance = context.instances[0]

    security_check_commands = [
        # Firewall checks
        "ufw status | grep -q 'Status: active'",  # UFW is active
        # Password policy
        "grep -q 'minlen=8' /etc/security/pwquality.conf",  # Minimum password length
        "grep -q '^password.*required.*pam_pwquality.so' /etc/pam.d/common-password",  # PAM module for password quality
        # SSH configuration
        "grep -q '^PermitRootLogin no' /etc/ssh/sshd_config",  # Root login disabled
        "grep -q '^PasswordAuthentication no' /etc/ssh/sshd_config",  # Password authentication disabled
        "grep -q '^X11Forwarding no' /etc/ssh/sshd_config",  # X11 forwarding disabled
        # File permissions and ownership
        "find / -perm 777 -type f | wc -l | grep -q '^0$'",  # No files with world-writable permissions
        "find /etc -type f -not -user root | wc -l | grep -q '^0$'",  # All files in /etc are owned by root
        # Security updates applied
        "grep -q '^deb.*security' /etc/apt/sources.list /etc/apt/sources.list.d/*",  # Security updates repository enabled (Debian/Ubuntu)
        "yum repolist enabled | grep -q 'updates'",  # Updates repository enabled (CentOS/RHEL)
        # Audit service running
        "systemctl is-active --quiet auditd",  # Audit daemon is active
        # Login banner
        "grep -q 'Authorized access only' /etc/issue",  # Login banner exists
        # Kernel parameters (sysctl)
        "sysctl net.ipv4.conf.all.rp_filter | grep -q '1'",  # Reverse path filtering
        "sysctl net.ipv4.conf.all.accept_source_route | grep -q '0'",  # Source routing disabled
        # Intrusion detection
        "systemctl is-active --quiet fail2ban",  # Fail2Ban is active
    ]

    for cmd in security_check_commands:
        out = exec_cmd(context, instance, cmd)
        result = out.stdout.strip()
        assert result == "", f"Security check failed for command: {cmd}"
