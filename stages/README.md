# Test stages

 Directory structure is organized following structure of test stages in CI/CD pipeline as defined in [main pipeline file](https://gitlab.com/nunet/nunet-infra/-/blob/develop/ci/templates/Auto-DevOps.gitlab-ci.yml?ref_type=heads). In case more stages are added or renamed, both directory structure and pipeline structure have to be refactored simulataneously. In case of questions, the structure of the test-suite is the main reference and all other repositories and definitions have to be aligned to definitions provided in this repository.

 Each stage folder is organized as follows:

```
 ├── {component_name} # (repository name)
    ├── features      # use case definitions in gherking (both automatic and manual!)
    ├── steps         # step definitions for automatic steps to be executed via pipelne
``````