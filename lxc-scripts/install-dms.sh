#!/usr/bin/env bash

# Function to select LXC instance
select_instance() {
    echo "Select LXC instance to install DMS on: (Pick a number)"
    # Replace the below command with your logic to list LXC instances
    lxc_list=$(lxc ls -c n| awk 'NR>2 {print $2}')
    select instance in $lxc_list; do
        if [ -n "$instance" ]; then
            echo "Selected instance: $instance"
            break
        else
            echo "Invalid selection. Please try again."
        fi
    done
}

# Function to select branch for DMS installation
select_branch() {
    echo "Select branch to install DMS from (default: develop):"
    read branch
    if [ -z "$branch" ]; then
        branch="develop"
    fi
}

# Function to update DMS from selected branch
update_dms() {
    echo "Updating DMS from branch $branch..."
    lxc exec "$instance" -- bash -ilc 'cd device-management-service || { echo "Directory not found"; exit 1; }; git checkout -f $branch && git pull origin $branch'
}

# Main function
main() {
    select_instance
    select_branch
    update_dms
    echo "Installing dependencies..."
    lxc exec "$instance" -- bash -ilc 'snap install go --classic'
    lxc exec "$instance" -- bash -ilc 'apt update && apt install -y nodejs npm build-essential libsystemd-dev iproute2 bc jq pandoc'
    echo "Running build script..."
    lxc exec "$instance" -- bash -ilc 'cd device-management-service && bash maint-scripts/build.sh'
    echo "Installing DMS..."
    lxc exec "$instance" -- bash -ilc 'apt install -f ./device-management-service/dist/nunet-dms_*.deb -y --allow-downgrades'
}

# Invoke main function
main
