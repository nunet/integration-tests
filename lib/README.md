This is a library folder to host code that is reused throughout the projects in this
repository.

Since this is a monorepo of sorts, many different languages can be hosted here. Each
language and domain should be contained in it's own folder, so that for instance bash
scripts don't get mixed with python scripts for instance, and different contexts should
also be kept in separate folders.
