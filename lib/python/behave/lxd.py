import subprocess
import os


def get_dms_instances_on_lxd(inventory_file):
    with open(inventory_file) as file_fp:
        instances = [line.strip() for line in file_fp.readlines() if line]
    return instances


def user_input_format(user_input):
    if user_input == "no":
        key_press = b"n\n"
        return key_press
    elif user_input == "yes":
        key_press = b"y\n"
        return key_press


def exec_command_lxd_instance(instance, command, ssh_key, user_input=None):
    key_press = user_input_format(user_input)
    if os.environ.get("BEHAVE_FUNCTIONAL_TESTS_EXEC_VERBOSE", "false") == "true":
        print(f"running '{command}' in '{instance}'...")
    return subprocess.run(
        [
            "ssh",
            "-o",
            "StrictHostKeyChecking=no",
            "-o",
            "IdentitiesOnly=yes",
            "-o",
            "PubKeyAuthentication=yes",
            "-i",
            ssh_key,
            f"root@{instance}",
            "--",
            command,
        ],
        input=key_press,
        capture_output=True,
        check=False,
        text=True,
    )


def assert_cmd(response, wantErr=False):
    """
    Args:
        response: subprocess.CompletedProcess object from command execution
        wantErr: boolean indicating if we expect an error

    Returns:
        tuple: (stdout decoded string, stderr decoded string)

    Raises:
        AssertionError: If output doesn't match expectations
    """
    stderr = getattr(response, 'stderr', '')
    if wantErr:
        assert stderr, "Wanted error, but got none"
    else:
        assert not stderr, f"Wanted no error, but got: {stderr}"


def exec_cmd(context, instance, command):
    ssh_key = context.config.userdata["ssh_key_file"]
    env = context.passphrase_key
    passphrase = context.passphrase_value
    return exec_command_lxd_instance(instance, f"{env}={passphrase} {command}", ssh_key)
