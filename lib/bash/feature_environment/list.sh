#!/usr/bin/env bash
set -euo pipefail

STAGE_NAME="${1:-}"
SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
PROJECT_ROOT="${SCRIPT_DIR//test-suite*/'test-suite'}"
STAGE_DIR="$PROJECT_ROOT/stages/$STAGE_NAME"

if [ ! -d "$STAGE_DIR" ]; then
	echo "Error: Stage directory '$STAGE_NAME' not found"
	exit 1
fi

find "$STAGE_DIR" -name "*.feature" -type f | grep -v "/.disabled/" | sort | sed "s~$PROJECT_ROOT/\(.*\)~\1~g"
