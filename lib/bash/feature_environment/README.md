This a collection of helper scripts to facilitate the execution of the functional tests in the feature environment, managed by [dms-on-lxd](infrastructure/dms-on-lxd).

- `install.sh` uses `requirements.txt` in `functional_tests` folder to create a virtual environment with the correct dependencies
- `run-standalone-tests.sh` runs all the standalone tests in each virtual machine
- `run-distributed-tests.sh` runs tests that require all virtual machines to run

# Requirements

- python 3
- lsof
- allure

# Environment Variables

The test scripts support the following optional environment variables:

- `FEATURE_ENVIRONMENT_DOCKER`: Set to "false" to run tests directly without Docker
container (default: "true")
- `BEHAVE_ENABLE_ALLURE`: Set to "true" to enable Allure test reporting (default: "false") 
- `BEHAVE_ENABLE_JUNIT`: Set to "true" to enable JUnit test reporting (default: "false")
- `DMS_ON_LXD_ENV`: Specify custom environment name to use alternate inventory files
- `CI_PROJECT_DIR`: Override project directory path (defaults to detected test-suite path)
- `BEHAVE_SKIP_ONBOARD_CHECK`: Skip device onboarding verification (default: "true")

# Usage

```shell
cd stages/functional_tests
source install.sh
source run-standalone-tests.sh
source run-distributed-tests.sh
```
