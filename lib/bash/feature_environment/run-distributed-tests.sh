#!/usr/bin/env bash
set -euo pipefail

stage="${1:?Usage: $0 <stage>}"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source "$SCRIPT_DIR/lib.sh"

get-distributed-test-results-dir
handle-docker-runner $SCRIPT_DIR/run-distributed-tests.sh "${@:1}"

echo running distributed tests...

echo get-behave-cmd $stage distributed "${@:2}"
get-behave-cmd $stage distributed "${@:2}"
echo "${BEHAVE_CMD[@]}"
pwd
if ! "${BEHAVE_CMD[@]}"; then
  touch failed_tests
fi
