import os
import base64

from selenium.webdriver.common.by import By
from selenium import webdriver

os.environ["MOZ_HEADLESS"] = "1"

username, password = (
    base64.b64decode(os.environ["DEFECTDOJO_CREDENTIALS"])
        .decode()
        .split(":")
)
engagement_id = os.environ["DEFECTDOJO_ENGAGEMENTID"]

driver = webdriver.Firefox()

print("opening page...")
driver.get("https://defectdojo.nunet.io")

print("logging in...")
username_element = driver.find_element("name", "username")
password_element = driver.find_element("name", "password")

username_element.send_keys(username)
password_element.send_keys(password)

login_button = driver.find_element(By.CSS_SELECTOR, ".btn.btn-success")
login_button.click()

print(f"getting quick report for engagement id {engagement_id}...")
driver.get(f"https://defectdojo.nunet.io/reports/quick?url=/engagements/{engagement_id}")

print("saving html source...")
# Save the current page as PDF
with open('defectdojo-quick-report.html', 'w') as f:
    f.write(driver.page_source)

driver.quit()
print("done")

