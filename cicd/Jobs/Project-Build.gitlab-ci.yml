variables:
  ALLOW_BUILD_FAIL: false

.project_build:
  rules:
    - if: $BUILD_DISABLED == "true" || $BUILD_DISABLED == "1" || $BUILD_IMAGE_ONLY == "true"
      when: never
    - when: always

Build:
  stage: build
  extends:
    - .project_build
  dependencies: []
  needs: []
  script:
    - echo "Building debian archives..."
    - |
      if ! bash maint-scripts/build.sh; then
        if [[ $ALLOW_BUILD_FAIL == "true" ]]; then
          exit 100
        else
          exit 1
        fi
      fi
  artifacts:
    untracked: false
    when: on_success
    expire_in: "30 days"
    paths:
      - dist/
  allow_failure:
    exit_codes: 100

build:osx:
  stage: build
  tags:
    - saas-macos-medium-m1
  image: macos-14-xcode-15
  extends:
    - .project_build
  dependencies: []
  needs: []
  variables:
    HOMEBREW_NO_AUTO_UPDATE: 1
  script:
    - echo "Building darwin binaries..."
    - |
      if ! bash maint-scripts/build-bsd.sh; then
        if [[ $ALLOW_BUILD_FAIL == "true" ]]; then
          exit 100
        else
          exit 1
        fi
      fi
  artifacts:
    untracked: false
    when: on_success
    expire_in: "30 days"
    paths:
      - dist/
  allow_failure:
    exit_codes: 100

.update-permalinks:
  stage: build
  image: bash:alpine3.21
  variables:
    PERMALINK_DEPLOY_SERVER: d.nunet.io
    PERMALINK_DEPLOY_USER: gitlab-runner
    PERMALINK_DEPLOY_FOLDER: /opt/public
    # PERMALINK_BUILD_CHANNELS is a space separated list: branch_name:release_channel
    PERMALINK_BUILD_CHANNELS: "main:latest release:stable"
    # PERMALINK_ARCH_OVERRIDE is a space separated list: original_arch:replace_arch_with
    # example: arm64:arm-osx
    PERMALINK_ARCH_OVERRIDE:  
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://gitlab.com
  secrets:
    PROD_SSH_KEY_B64:
      vault: devops/ssh-keys/prod/gitlab-runner/base64-key@nunet
      token: $VAULT_ID_TOKEN
      file: false
  before_script:
    - apk add rsync openssh
  script:
    - echo $PROD_SSH_KEY_B64 | base64 -d > prod_ssh_key && chmod 600 prod_ssh_key && ls -latrh prod_ssh_key
    # in next step it would have been simpler to hardcode the build channel to branch map
    # however this implementation functions as a way to quickly implement other channels,
    # but must importantly is serves as a seam for the test-suite testing pipeline
    - |
      for build_channel_cfg in $PERMALINK_BUILD_CHANNELS; do
        target_branch=$(cut -d ':' -f 1 <<<"$build_channel_cfg")
        build_channel_candidate=$(cut -d ':' -f 2 <<<"$build_channel_cfg")
        if [[ "$target_branch" == "$CI_COMMIT_REF_NAME" ]]; then
          build_channel="$build_channel_candidate"
        fi
      done
      if [[ "$build_channel" == "" ]]; then
        echo no build channel for branch $CI_COMMIT_REF_NAME
        exit 1
      fi
      echo using build channel $build_channel
    - |
      shopt -s globstar nullglob
      cd dist
      for file in *.deb *.zip; do
        # Extract version, architecture, and file type
        if [[ $file =~ nunet-dms_(v)?(0\.5\.0)(-dirty)?(-[0-9]+(-[a-f0-9]+)?)?_(amd64|arm32|arm64)\.(deb|zip) ]]; then
          version=${BASH_REMATCH[2]}  # Capture the version
          ext=${BASH_REMATCH[7]}      # Capture the file extension

          # capture and optionally override cpu architecture name
          if [[ "$PERMALINK_ARCH_OVERRIDE" != "" ]]; then
            original_arch=${BASH_REMATCH[6]}
            arch="$original_arch"  # if no override matches, keep original arch
            for arch_override in $PERMALINK_ARCH_OVERRIDE; do
              target_arch=$(cut -d ':' -f 1 <<<"$arch_override")
              replace_arch_with=$(cut -d ':' -f 2 <<<"$arch_override")
              if [[ "$target_arch" == "$original_arch" ]]; then
                arch="$replace_arch_with"
              fi
            done
          else
            arch=${BASH_REMATCH[6]}
          fi
      
          new_name="nunet-dms-${version}-${arch}-${build_channel}.${ext}"
          versionless_new_name="nunet-dms-${arch}-${build_channel}.${ext}"
          echo renaming \'$file\' to \'$new_name\'
          mv "$file" "$new_name"
          echo adding versionless permalink
          cp "$new_name" "$versionless_new_name"
        else
          echo "Skipping: $file (does not match pattern)"
          echo $file >> /tmp/file_pattern_mismatch
        fi
      done
      cd $CI_PROJECT_DIR
    - |
      rsync -va \
        --rsh "ssh -o StrictHostKeyChecking=no -i prod_ssh_key" \
        $CI_PROJECT_DIR/dist/ \
        $PERMALINK_DEPLOY_USER@$PERMALINK_DEPLOY_SERVER:$PERMALINK_DEPLOY_FOLDER
    - |
      if [[ -f /tmp/file_pattern_mismatch ]]; then
        echo some files failed permalink pattern matching
        cat /tmp/file_pattern_mismatch
        exit 1
      fi
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^(release|release\/.+|main)$/ && $CI_PIPELINE_SOURCE =~ /^(merge_request_event|push|web)$/
      when: always

update-permalinks:linux:
  extends:
    - .update-permalinks
  needs:
    - job: Build
      artifacts: true

update-permalinks:osx:
  extends:
    - .update-permalinks
  needs:
    - job: build:osx
      artifacts: true
  variables:
    PERMALINK_ARCH_OVERRIDE: arm64:arm-osx


