.publish_reports:
  stage: .post
  image: alpine:3.19.1
  allow_failure: true
  when: always
  variables:
    REPORTS_TO_UPLOAD: gl-code-quality-report.html gl-table-scanning-report.html defectdojo-quick-report.html
    CI_REPORTS_DISABLE_MR_ANNOTATION: false
    CI_REPORTS_TARGET_PIPELINE_ID: $CI_PIPELINE_ID
    CI_REPORTS_TARGET_BRANCH: $CI_COMMIT_REF_NAME
    CI_REPORTS_TARGET_PROJECT: $CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME
    CI_REPORTS_DIR: /usr/share/nginx/ci-reports
    CI_REPORTS_URL: ci.nunet.io
    CI_REPORTS_SSH_USER: gitlab-runner
    CI_REPORTS_URL_SUBPATH: reports
    CI_REPORTS_PUBLICATION_SUBPATH: $CI_REPORTS_TARGET_PROJECT/$CI_REPORTS_TARGET_BRANCH
    CI_REPORTS_BRANCH_URI: https://$CI_REPORTS_URL/$CI_REPORTS_URL_SUBPATH/$CI_REPORTS_PUBLICATION_SUBPATH
    CI_REPORTS_DESTINATION_PATH: $CI_REPORTS_DIR/$CI_REPORTS_PUBLICATION_SUBPATH/$CI_REPORTS_TARGET_PIPELINE_ID
    CI_REPORTS_FINAL_URI: $CI_REPORTS_BRANCH_URI/$CI_REPORTS_TARGET_PIPELINE_ID
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://gitlab.com
  secrets:
    CI_REPORTS_SSH_KEY_B64:
      vault: devops/ssh-keys/ci-reports/base64-key@nunet
      token: $VAULT_ID_TOKEN
      file: false
  artifacts:
    reports:
      dotenv: .publish-reports.env
  resource_group: publish-reports-$CI_REPORTS_TARGET_PIPELINE_ID
  before_script:
    - echo "CI_REPORTS_FINAL_URI=$CI_REPORTS_FINAL_URI" >> .publish-reports.env
  script:
    - apk add rsync openssh curl jq
    - echo uploading $REPORTS_TO_UPLOAD to server $CI_REPORTS_URL on "$CI_REPORTS_DESTINATION_PATH" using $CI_REPORTS_SSH_USER
    - echo $CI_REPORTS_SSH_KEY_B64 | base64 -d > ci_reports_ssh_key && chmod 600 ci_reports_ssh_key
    - ssh -o StrictHostKeyChecking=no -i ci_reports_ssh_key "$CI_REPORTS_SSH_USER"@"$CI_REPORTS_URL" mkdir -p "$CI_REPORTS_DESTINATION_PATH"
    - |
      rsync -va \
        --rsh "ssh -i ci_reports_ssh_key" \
        $REPORTS_TO_UPLOAD \
        "$CI_REPORTS_SSH_USER"@"$CI_REPORTS_URL":"$CI_REPORTS_DESTINATION_PATH"
    - echo reports for this pipeline available at "$CI_REPORTS_FINAL_URI"
    - |-
      if [[ $CI_PIPELINE_SOURCE == "merge_request_event" ]]; then
        # this captures all merge request comments and append username + comment body
        gitlab_api_notes_url="https://gitlab.com/api/v4/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes"
        previous_comments=$(
          curl --location --request GET \
            "$gitlab_api_notes_url" \
            --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" | jq -r '.[] | .author.username + .body'
        )
        # this checks if HTML Reports comment is already in the merge request comments and skips comment
        # otherwise add comment to merge request. The user is in the format group_7888999_bot_3ecea46db463cc9c88cbce63b1aabcde
        comment_body_prefix="HTML Reports for pipeline $CI_REPORTS_TARGET_PIPELINE_ID available at"
        if echo "$previous_comments" | awk "/group_[0-9]+_bot_[a-zA-Z0-9]+$comment_body_prefix/ {found=1; exit} END {if (!found) exit 1}"; then
          echo already commented on merge request for pipeline $CI_REPORTS_TARGET_PIPELINE_ID. Skipping comment...
        elif [[ "$CI_REPORTS_DISABLE_MR_ANNOTATION" != "true" ]]; then
          curl --location --request POST \
            "$gitlab_api_notes_url" \
            --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
            --header "Content-Type: application/json" \
            --data-raw "{ \"body\": \"$comment_body_prefix $CI_REPORTS_FINAL_URI\" }"
        else
          echo not in a merge request. Skipping comment...
        fi
      fi

